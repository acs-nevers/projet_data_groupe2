//Geolocation of user
var lat = 0,
    lon = 0,
    latD = 0,
    lonD = 0;
var geoExecuted = false;
var map,
    markerGroup,
    currentPos,
    route;

if (navigator.geolocation)
    var watchId = navigator.geolocation.watchPosition(successCallback, errorCallback, {enableHighAccuracy:true});
else
    alert("Votre navigateur ne prend pas en compte la géolocalisation HTML5");

function successCallback(position){
    if (!geoExecuted) {
        lat = position.coords.latitude;
        lon = position.coords.longitude;
        displayMap(lat, lon);
        geoExecuted = true
    }
};

function errorCallback(error){
    switch(error.code){
        case error.PERMISSION_DENIED:
            alert("Veuillez autoriser l'accès à votre position dans les paramètres de votre navigateur et/ou de votre appareil.");
            break;
        case error.POSITION_UNAVAILABLE:
            alert("L'emplacement de l'utilisateur n'a pas pu être déterminé.");
            break;
        case error.TIMEOUT:
            alert("Le service n'a pas répondu à temps.");
            break;
        }
};

function stopWatch(){
    navigator.geolocation.clearWatch(watchId);
}


// Leaflet
function displayMap(lat, lon) {
    map = L.map('map').setView([lat, lon], 17);

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
        attribution: '© OpenStreetMap contributors',
        id: 'mapbox.streets',
        accessToken: 'pk.eyJ1IjoieWthcnRzYWsiLCJhIjoiY2puaGdsc3J4MGNuNzN3dGwxMWxmMmVrdCJ9.0fI9siwAMxB-q4VOrcA0uQ',
        maxZoom: 18,
        minZoom: 2,
    }).addTo(map);

    // Create layer group to put markers on
    markerGroup = L.layerGroup().addTo(map);

    // Add maker to current position
    currentPos = L.marker([lat, lon]).addTo(markerGroup);
}

// Ajax
function getAjaxCoords() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;
            var coords = JSON.parse(response);
            if (coords == []) {
                alert('Aucune donnée récupérée');
            } else {
                distances = getDistances(coords);
                closest = getClosest(distances);
                routing(closest, coords);
                displayInfo(closest, coords);
            }
        }
    };
    xhttp.open("GET", "./map/ajax", true);
    xhttp.send();
}

// Leaflet Routing
function routing(closest, coords) {
    // Destination
    latD = coords[closest].latitude;
    lonD = coords[closest].longitude;

    // Clear markers
    markerGroup.clearLayers();

    // Remove route if already defined
    if (route != undefined) {
        map.removeControl(route);
    }
    // Display route between current pos and destination
    route = L.Routing.control({
        waypoints: [
            L.latLng(lat, lon),
            L.latLng(latD, lonD)
        ],
        routeWhileDragging: false,
        language: 'fr'
    }).addTo(map);
}

// Get distances
function getDistances(coordsArray) {
    var distances = [];
    for (var i = 0; i < coordsArray.length; i++) {
        var deltaLat = Math.abs(lat - coordsArray[i].latitude);
        var deltaLon = Math.abs(lon - coordsArray[i].longitude);
        var dist = Math.sqrt(Math.pow(deltaLat, 2) + Math.pow(deltaLon, 2));
        distances.push(dist);
    }
    return distances;
}

// Get closest defibrilator
function getClosest(distances) {
    var min = distances.min();
    return distances.indexOf(min);
}

// Display closest def info
function displayMeta() {
    var metaInfo = document.getElementById('def-meta-info');

    metaInfo.classList.toggle('visible');
}

function displayInfo(closest, coords) {
    var defName = document.getElementById('def-name');
    var defAddress =  document.getElementById('def-address');
    var defSchedule = document.getElementById('def-schedule');

    defName.innerHTML = coords[closest].name;
    defAddress.innerHTML = coords[closest].address + ' '
        + coords[closest].code + ' ' + coords[closest].city;

    var schedule = coords[closest].schedules;

    defSchedule.innerHTML = convertSchedule(schedule);
}

function convertSchedule(string) {
    var array = string.split(',');
    var schedule = '';
    var nulls = 0;

    for (var i = 0; i < array.length; i++) {
        if (i % 4 === 0) {
            if (i !== 0) {
                schedule += '<br>';
            }
            switch (i) {
                case 0:
                    schedule += '<span class="day">Lundi :</span> ';
                    break;
                case 4:
                    schedule += '<span class="day">Mardi :</span> ';
                    break;
                case 8:
                    schedule += '<span class="day">Mercredi :</span> ';
                    break;
                case 12:
                    schedule += '<span class="day">Jeudi :</span> ';
                    break;
                case 16:
                    schedule += '<span class="day">Vendredi :</span> ';
                    break;
                case 20:
                    schedule += '<span class="day">Samedi :</span> ';
                    break;
                case 24:
                    schedule += '<span class="day">Dimanche :</span> ';
                    break;
                default:
                    break;
            }
        }
        if (array[i] == 'NULL'){
            nulls++;
        }

        if (i % 4 === 1 && nulls === 0 || i % 4 === 3 && nulls === 2) {
            schedule += '-';
        }

        if (i % 4 === 2) {
            schedule += ' ';
        }

        if (array[i] === 'NULL' && nulls % 4 !== 0) {

        } else if (nulls % 4 === 0 && nulls !== 0) {
            schedule += 'Fermé';
        } else if(array[i] == 'Non renseigné') {
            schedule = 'Non renseignés';
        } else {
            schedule += array[i];
        }
        if (i % 4 === 3) {
            nulls = 0;
        }
    }

    return schedule;
}

function go() {
    getAjaxCoords();
}

// Additionnal functions
Array.prototype.max = function() {
  return Math.max.apply(null, this);
};

Array.prototype.min = function() {
  return Math.min.apply(null, this);
};

// Code to execute
var goButton = document.getElementById('run');

goButton.onclick = go;

document.getElementById('def-name').onclick = displayMeta;
