var research = document.getElementById('research');
var searchBar = document.getElementById('search-field');

function reveal_hide(){
    if(research.classList.contains('active')){
        research.classList.remove('active');
    }
    else{
        research.classList.add('active');
    }
}

window.addEventListener ('load', function(){
    setTimeout(reveal_hide, 500);
});

function display(){
    research.style.display = 'none';
}

searchBar.addEventListener('click', display);
