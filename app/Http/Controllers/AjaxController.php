<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

class AjaxController extends Controller {
    public function map(){
        $coords = \DB::select('SELECT name, address, code, city, latitude, longitude, schedules FROM defibrillators');
        echo json_encode($coords);
    }
    public function searchAjax(){
        $search = Input::get('search');
        $search = trim($search);
        $search = stripslashes($search);
        $search = htmlspecialchars($search);
        $results = \DB::select("SELECT id, arrondissement, type, name, address, code, city, def_number, latitude, longitude FROM `defibrillators` WHERE arrondissement LIKE '%:search%' OR type LIKE '%$search%' OR name LIKE '%$search%' OR address LIKE '%$search%' OR code LIKE '%$search%' OR city LIKE '%$search%' OR latitude LIKE '%$search%' OR longitude LIKE '%$search%'");
        echo json_encode($results);
    }
}

