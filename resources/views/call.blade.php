@extends('default')

@section('title')
	Informations
@endsection

@section('content')
<!--section with content-->
<main class="backgrd">
    <!--section title of page-->
    <div class="title">
        <h2>À Communiquer Aux Urgences</h2>
    </div>
    <!--section of every division for the page-->
    <section class="content">
        <!--beginning of part-->
        <article class="part">
            <!--title part-->
            <div class="title">
                <h2>Vous concernant</h2>
            </div>
            <!--description part-->
            <div class="info">
                <article class="sub-info">
                    <ol>
                        <li>Nom Prénom</li>
                        <li>Numéro de Téléphone</li>
                        <li>Adresse Accident</li>
                    </ol>
                </article>
            </div>
        </article>
			<!--end of part-->

			<!--beginning of part-->
		<article class="part">
            <!--title part-->
            <div class="title">
				<h2>Concernant la victime</h2>
            </div>
            <!--description part-->
            <div class="info">
                <article class="sub-info">
                    <ol>
                        <li>Sexe</li>
                        <li>Âge approximatif</li>
						<li>Décrire l'état de la victime</li>
						<li>Antécédents médicaux (si connus)</li>
						<li>Décrire les étapes déjà appliquées</li>
						<li>Répondre aux questions du SAMU</li>
                        <li>Ne pas raccrocher avant que l'on vous y invite</li>
                    </ol>
                </article>
            </div>
        </article>
			<!--end of part-->
		</section>
		<!--end of section division-->
	</main>
@endsection
