@extends('default')

@section('title')
P.A.S
@endsection

@section('content')
<!--section with content-->
<main class="main backgrd">
    <!--section title of page-->
    <div class="title">
        <h2>protéger alerter secourir - p. a. s.</h2>
    </div>
    <!--section of every division for the page-->
    <section class="content">
        <!--beginning of part-->
        <article class="part">
            <!--title part-->
            <div class="title">
                <h2>Protéger</h2>
            </div>
            <!--description part-->
            <div class="desc">
                <summary class="part summary">Liste des étapes à faire avant tout</summary>
                <article class="sub-part article">
                    <ol>
                        <li>Bien observer les lieux</li>
                        <li>Sécuriser</li>
                        <li>Demander à un témoin de garder à distance les curieux</li>
                    </ol>
                </article>
            </div>
        </article>
        <!--end of part-->

        <!--beginning of part-->
        <article class="part">
            <!--title part-->
            <div class="title">
                <h2>Alerter</h2>
            </div>
            <!--description part-->
            <div class="desc">
                <summary class="part summary">Liste des numéros des secours</summary>
                <article class="sub-part article">
                    <ul>
                        <a href="{{ url('tel:15')}}">
                            <li>Samu : 15</li>
                        </a>
                        <a href="{{ url('tel:18')}}">
                            <li>Pompiers : 18</li>
                        </a>
                        <a href="{{ url('tel:112')}}">
                            <li>Numéro Européen : 112</li>
                        </a>
                    </ul>
                </article>
            </div>
        </article>
        <!--end of part-->

        <!--beginning of part-->
        <article class="part">
            <!--title part-->
            <div class="title">
                <h2>Secourir</h2>
            </div>
            <!--description part-->
            <div class="desc">
                <summary class="part summary">Liste des gestes de Premiers secours</summary>
                <article class="sub-part article">
                    <p>MASSAGE CARDIAQUE : 120 compressions par minute, soit 2 par seconde.</p>
                    <p>DEFIBRILLATION : À l’aide d’un défibrillateur automatisé externe. Très simple
                        d’utilisation, cet appareil guide vocalement, étape par étape, et garantit une
                        utilisation sans risque.</p>
                </article>
            </div>
        </article>
        <!--end of part-->
    </section>
    <!--end of section division-->
</main>
@endsection

@section('scripts')
    <script src="{{ asset('./js/p-a-s.js') }}"></script>
@endsection

