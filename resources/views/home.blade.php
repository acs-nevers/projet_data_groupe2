@extends('default')

@section('title')
    Home
@endsection

@section('content')
    <!--section with content-->
    <section id="home">
        <article id="title" class="informations left">
            <p>Localisez le défibrillateur le plus proche de vous.</p>
        </article>
        <!--button section to go on the map-->
        <a id="sos-button" href="{{ url('/map') }}">
            <h1>SOS</h1>
        </a>
        <!--end of button section-->
        <img src="{{ url('/img/informations.png') }}" class="right" id="inf">
        <article id="informations" class="informations right">
						<p>En cliquant sur <img src="{{ url('/img/picto-soin.png') }}" alt="Icône premiers secours" class="icon-footer"> vous pourrez trouver les étapes à effectuer en cas d'accidents.</p>
            <p>En cliquant sur <img src="{{ url('/img/Emergency.png') }}" alt="Icône numéros d'urgence" class="icon-footer"> vous pourrez trouver tous les éléments à communiquer aux urgences.</p>
        </article>
    </section>

@endsection

@section('scripts')
    <script src="{{ url('js/home.js') }}"></script>
@endsection
